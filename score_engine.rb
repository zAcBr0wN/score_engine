class ScoreEngine

    attr_accessor :frames, :bonus

    def initialize(frames)
        self.frames = frames
        self.bonus = frames.last if frames.count == 11
    end

    def strike?(frame)
        frame.nil? ? false : frame[0] == 10
    end

    def sum_frame(frame)
        frame.nil? ? 0 : frame.inject(:+)
    end

    def spare?(frame)
        frame.nil? ? false : !strike?(frame) && sum_frame(frame) == 10
    end

    def score 
        score = 0
        frames.each_with_index do |frame, frame_index|
            score += sum_frame(frame) if !strike?(frame) && !spare?(frame) && frame != self.bonus
            score += calculate_strike_score(frame, frame_index) if strike?(frame) && frame != self.bonus
            score += calculate_spare_score(frame, frame_index) if spare?(frame) && frame != self.bonus
        end
        score
    end

    private 

    def calculate_strike_score(frame, frame_index)
        if strike?(frames[frame_index+1]) && frames[frame_index+1] != self.bonus
            frame[0] + frames[frame_index+1][0] + frames[frame_index+2][0]
        else
            frame[0] + sum_frame(frames[frame_index+1])
        end
    end

    def calculate_spare_score(frame, frame_index)
        frames[frame_index+1] ? sum_frame(frame) + frames[frame_index+1][0] : sum_frame(frame)
    end
end


