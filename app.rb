require 'thor'
require './score_engine'
require 'json'

module BowlingApp
    class CLI < Thor
        desc "score FRAMES", "Calculate score for given FRAMES"
        def score(frames)
            frames = JSON.parse(frames)
            score_engine = ScoreEngine.new(frames.to_a)
            puts "Score: #{score_engine.score}"
        end
    end
end

BowlingApp::CLI.start(ARGV)
