# ScoreEngine

A sample CLI app for Strongind interview process for scoring a bowling game given a series of frame scores

## Getting Started

Install required dependencies. I've used `thor` for the heavy lifting of the CLI portion of this app, and (naturally) `rspec` for testing. 

Use `bundler` to install the dependencies from the Gemfile. If you don't already have bundler installed, `gem install bundler`

```
bundler install
```

### Running Tests

The tests are very straight forward and located in the `spec` dir. 

`rspec spec`

## Scoring a Bowling Game

The app works by calculating the frames for a given game based on the requirements of the user story. 

Simply provide the series of frae scores as an argument to the app

```
ruby app.rb score [[3,4], [10], [7,3]]
```
