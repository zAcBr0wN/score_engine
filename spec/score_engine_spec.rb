require 'spec_helper.rb'
require './score_engine.rb'

describe ScoreEngine do
  let!(:engine) { ScoreEngine.new([[1,1]]) }

  it "should accept an array of frames" do
    engine = ScoreEngine.new([[1,1], [10]])
    expect(engine.frames).to be == [[1,1], [10]]
  end

  it "should sum a frame correctly" do
    engine = ScoreEngine.new([[5,5]])
    expect(engine.sum_frame(engine.frames[0])).to be == 10
  end

  it "should return zero if summing a nil frame" do
    engine = ScoreEngine.new([[5,5]])
    expect(engine.sum_frame(engine.frames[1])).to be == 0
  end

  it "should correctly determine when a spare is rolled" do
    engine = ScoreEngine.new([[5,5]])
    expect(engine.spare?(engine.frames[0])).to be true
  end

  it "should correctly determine when a strike is rolled" do
    engine = ScoreEngine.new([[10]])
    expect(engine.strike?(engine.frames[0])).to be true
  end

  it "should not return a spare if a strike is rolled" do
    engine = ScoreEngine.new([[10]])
    expect(engine.spare?(engine.frames[0])).to be false
  end

  it "should score a spare correctly" do
    engine = ScoreEngine.new([[4,6],[5,0]])
    expect(engine.score).to be == 20
  end

  it "should score a strike correctly" do 
    engine = ScoreEngine.new([[10,0],[5,4]])
    expect(engine.score).to be == 28
  end
  
  it "should score two rolls without spare or strike correctly" do
    engine = ScoreEngine.new([[4,4]])
    expect(engine.score).to be == 8
  end
      
  it "should score multiple spares correctly" do
    engine = ScoreEngine.new([[5,5],[2,8],[3,7],[6,4]])
    expect(engine.score).to be == 51
  end
  
  it "should calculate bonus if spare is rolled" do
    engine = ScoreEngine.new([[3,3],[3,3],[3,3],[3,3],[3,3],[3,3],[3,3],[3,3],[3,3],[0,10],[6,0]])
    expect(engine.score).to be == 70
  end
  
  it "should should accurately score with no spares or strikes" do
    engine = ScoreEngine.new([[3,1],[2,0],[3,5],[3,6],[4,0],[0,5],[7,0],[8,1],[2,0],[0,0]])
    expect(engine.score).to be == 50
  end
  
  it "should correctly score if all spares" do
    engine = ScoreEngine.new([[3,7],[3,7],[3,7],[3,7],[3,7],[3,7],[3,7],[3,7],[3,7],[3,7],[10,0]])
    expect(engine.score).to be == 137
  end

  it "should correctly score 300 if every roll is a strike" do
    engine = ScoreEngine.new([[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]])
    expect(engine.score).to be == 300
  end

  it "should score correctly for all strikes except last roll no strike or spare" do
    engine = ScoreEngine.new([[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[5,3]])
    expect(engine.score).to be == 261
  end
end